package com.plotter;

import javafx.application.Platform;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import static jdk.nashorn.internal.runtime.regexp.joni.ast.Node.*;

public class PlotterTemp{

    private LineChart<Number, Number> lineChartTemperature;
    private XYChart.Series<Number, Number> seriesTemp;
    private double time = 0.0;
    private NumberAxis xAxisTemp;
    private NumberAxis yAxisTemp;
    private Label lbl;

    public PlotterTemp(VBox root,Label _lbl) {
    	// instantiating axis objects
        xAxisTemp = new NumberAxis();
        xAxisTemp.setLabel("Tick");
        yAxisTemp = new NumberAxis(0, 50, 5);
        yAxisTemp.setLabel("�C");

        // setting the static label
        this.lbl = _lbl;

        // setting up the chart and series
        lineChartTemperature = new LineChart<Number, Number>(xAxisTemp, yAxisTemp);
        seriesTemp = new XYChart.Series<Number, Number>();
        lineChartTemperature.getData().add(seriesTemp);

        // applying the chart to the layout
        root.getChildren().add(lineChartTemperature);
    }

    /** method for updating the series as the data comes in*/
    public void updatePlotTemp(double num) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                seriesTemp.getData().add(new XYChart.Data<>(time+=1, num));
                lbl.setText("Current temperature: " + num + " �C");
            }
        });
    }


	public LineChart<Number, Number> getLineChartTemperature() {
		return lineChartTemperature;
	}


	public void setLineChartTemperature(LineChart<Number, Number> lineChartTemperature) {
		this.lineChartTemperature = lineChartTemperature;
	}


	public XYChart.Series<Number, Number> getSeriesTemp() {
		return seriesTemp;
	}


	public void setSeriesTemp(XYChart.Series<Number, Number> seriesTemp) {
		this.seriesTemp = seriesTemp;
	}


	public NumberAxis getxAxisTemp() {
		return xAxisTemp;
	}


	public void setxAxisTemp(NumberAxis xAxisTemp) {
		this.xAxisTemp = xAxisTemp;
	}


	public NumberAxis getyAxisTemp() {
		return yAxisTemp;
	}


	public void setyAxisTemp(NumberAxis yAxisTemp) {
		this.yAxisTemp = yAxisTemp;
	}


	public Label getLbl() {
		return lbl;
	}


	public void setLbl(Label lbl) {
		this.lbl = lbl;
	}
    
    
}
