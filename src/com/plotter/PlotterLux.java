package com.plotter;

import javafx.application.Platform;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;

public class PlotterLux {

    private LineChart<Number, Number> lineChartLux;
    private XYChart.Series<Number, Number> seriesLux;
    private double time = 0.0;
    private NumberAxis xAxisLux;
    private NumberAxis yAxisLux;
    private Label lbl;

    public PlotterLux(VBox root, Label _lbl){
    	// instantiating axis objects
        xAxisLux = new NumberAxis();
        xAxisLux.setLabel("Tick");
        yAxisLux = new NumberAxis(0,1200,100);
        yAxisLux.setLabel("Lux");
        
        // setting the static label
        this.lbl = _lbl;

        // setting up the chart and series
        lineChartLux = new LineChart<Number, Number>(xAxisLux, yAxisLux);
        seriesLux = new XYChart.Series<Number, Number>();
        lineChartLux.getData().add(seriesLux);

        // applying the chart to the layout
        root.getChildren().add(lineChartLux);
    }

    /** method for updating the series as the data comes in*/
    public void updatePlotLux(double num) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                seriesLux.getData().add(new XYChart.Data<>(time+=1, num));
                lbl.setText("Current lux: " + num);
            }
        });
    }

	public LineChart<Number, Number> getLineChartLux() {
		return lineChartLux;
	}

	public void setLineChartLux(LineChart<Number, Number> lineChartLux) {
		this.lineChartLux = lineChartLux;
	}

	public XYChart.Series<Number, Number> getSeriesLux() {
		return seriesLux;
	}

	public void setSeriesLux(XYChart.Series<Number, Number> seriesLux) {
		this.seriesLux = seriesLux;
	}

	public NumberAxis getxAxisLux() {
		return xAxisLux;
	}

	public void setxAxisLux(NumberAxis xAxisLux) {
		this.xAxisLux = xAxisLux;
	}

	public NumberAxis getyAxisLux() {
		return yAxisLux;
	}

	public void setyAxisLux(NumberAxis yAxisLux) {
		this.yAxisLux = yAxisLux;
	}

	public Label getLbl() {
		return lbl;
	}

	public void setLbl(Label lbl) {
		this.lbl = lbl;
	}
    
    
}
