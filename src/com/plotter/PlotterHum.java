package com.plotter;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class PlotterHum {

    private LineChart<Number, Number> lineChartHumidity;
    private XYChart.Series<Number, Number> seriesHum;
    private double time = 0.0;
    private NumberAxis xAxisHum;
    private NumberAxis yAxisHum;
    private Label lbl;

    public PlotterHum(VBox root, Label _lbl){
        // instantiating axis objects
        xAxisHum = new NumberAxis();
        xAxisHum.setLabel("Tick");
        yAxisHum = new NumberAxis(0, 100, 10);
        yAxisHum.setLabel("%");
        
        // setting the static label
        this.lbl = _lbl;

        // setting up the chart and series
        lineChartHumidity = new LineChart<Number, Number>(xAxisHum, yAxisHum);
        seriesHum = new XYChart.Series<Number, Number>();
        lineChartHumidity.getData().add(seriesHum);

        // applying the chart to the layout
        root.getChildren().add(lineChartHumidity);
    }

    /** method for updating the series as the data comes in*/
    public void updatePlotHum(double num) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                seriesHum.getData().add(new XYChart.Data<>(time+=1, num));
                lbl.setText("Current humidity: " + num + "%");
            }
        });
    }

	public LineChart<Number, Number> getLineChartHumidity() {
		return lineChartHumidity;
	}

	public void setLineChartHumidity(LineChart<Number, Number> lineChartHumidity) {
		this.lineChartHumidity = lineChartHumidity;
	}

	public XYChart.Series<Number, Number> getSeriesHum() {
		return seriesHum;
	}

	public void setSeriesHum(XYChart.Series<Number, Number> seriesHum) {
		this.seriesHum = seriesHum;
	}


	public NumberAxis getxAxisHum() {
		return xAxisHum;
	}

	public void setxAxisHum(NumberAxis xAxisHum) {
		this.xAxisHum = xAxisHum;
	}

	public NumberAxis getyAxisHum() {
		return yAxisHum;
	}

	public void setyAxisHum(NumberAxis yAxisHum) {
		this.yAxisHum = yAxisHum;
	}

	public Label getLbl() {
		return lbl;
	}

	public void setLbl(Label lbl) {
		this.lbl = lbl;
	}
    
    
}
