package com.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import com.fazecast.jSerialComm.SerialPort;
import com.application.Main;
import com.serial.SerialConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import com.serial.SerialDataTransferHumidity;
import com.serial.SerialDataTransferLux;
import com.serial.SerialDataTransferTemperature;
import com.storage.DataStorage;
import com.storage.chart.DBDataPlotterController;

public class ConnectController {

	/** FXML fields*/
    @FXML
    private ComboBox<String> choiceBoxPort;
    @FXML
    private Button buttonGo;
    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonStop;
    @FXML
    private ComboBox<String> comboBoxDB;
    @FXML
    private DatePicker datePickerDB;
    @FXML
    private ComboBox<DataStorage> comboBoxStartTime;
    @FXML
    private ComboBox<DataStorage> comboBoxEndTime;
    @FXML
    private ProgressBar progressIndicator;

    /** non-fxml fields */
    private Label labelTemp;
    private Label labelHum;
    private Label labelLux;
    private VBox boxTemp;
    private VBox boxHum;
    private VBox boxLux;
    private SerialPort chosenPort;
    private SerialConnection sc;
    private SerialDataTransferTemperature sdtTemp;
    private SerialDataTransferHumidity sdtHum;
    private SerialDataTransferLux sdtLux;
    private Object lock;
    private Scene mainScene;
    private final String temp = "Temperature";
    private final String hum = "Humidity";
    private final String lux = "Lux";
    private ObservableList<DataStorage> obsList = FXCollections.observableArrayList();
    private List<DataStorage> _list;
    
    /** init method*/
    @FXML
    void initialize(){
    	sc = new SerialConnection();
    	choiceBoxPort.setItems(sc.getObservablePortList());
    	lock = new Object();
    	comboBoxDB.getItems().addAll(temp, hum, lux);
    	buttonStop.setDisable(true);
    	_list = new ArrayList<DataStorage>();
    }
    
    /** Method used to get data from database*/
    @FXML
    void buttonGetFromDBOnAction(ActionEvent event) {
    	// clearing boxes before loading data
    	if (!comboBoxStartTime.getItems().isEmpty() || !comboBoxEndTime.getItems().isEmpty()){
    	comboBoxStartTime.getItems().clear();
    	comboBoxEndTime.getItems().clear();
    	}
    	
    	// temp list for storing data
    	List<DataStorage> list = new ArrayList<DataStorage>();
    	
    	// hibernate objects for DB
    	final Session session;
		Transaction transaction = null;
		session = Main.getSession();
		
		try{
			transaction = session.beginTransaction();
			// getting list by created criteria
			list = session.createCriteria(DataStorage.class).add(Restrictions.eq("type", comboBoxDB.getSelectionModel().getSelectedItem())).list();
			transaction.commit();
		}catch(HibernateException e){
			e.printStackTrace();
			transaction.rollback();
		}
		
		// populating lists with data using the second criteria (by date)
		for (DataStorage ds: list){
			if (ds.getDateTime().getMonth() == datePickerDB.getValue().getMonth() 
					&& ds.getDateTime().getDayOfWeek() == datePickerDB.getValue().getDayOfWeek()){
				//obsList.add(ds.getDateTime().getHour()+":"+ds.getDateTime().getMinute()+":"+ds.getDateTime().getSecond());
				obsList.add(ds);
				_list.add(ds);
			}
		}
		
		// setting measurement by time into combo boxes
		comboBoxStartTime.setItems(obsList);
		comboBoxEndTime.setItems(obsList);
    }
    
    @FXML
    void checkBoxDarkThemeOnAction(ActionEvent event) {
    	//mainScene.getStylesheets().clear();
		
		sdtTemp.getPlotterTemp().getLineChartTemperature().applyCss();
		sdtHum.getPlotterHum().getLineChartHumidity().applyCss();
		sdtLux.getPlotterLux().getLineChartLux().applyCss();
    }

    /** Method used to plot to a chart 
     * using data from database*/
    @FXML
    void buttonPlotOnAction(ActionEvent event) {
    	// opening a new window with a chart
    	try{
    		Stage stage = new Stage();
    		FXMLLoader loader = new FXMLLoader();
    		AnchorPane form = loader.load(getClass().getResource("/com/storage/chart/DBDataPlotter.fxml").openStream());
    		DBDataPlotterController controller = (DBDataPlotterController) loader.getController();
    		controller.setList(_list);
    		controller.setStartTimeSelected(comboBoxStartTime.getSelectionModel().getSelectedItem());
    		controller.setEndTimeSelected(comboBoxEndTime.getSelectionModel().getSelectedItem());
    		Scene scene = new Scene(form, 640, 480);
    		stage.setScene(scene);
    		stage.setTitle("Plot");
    		stage.toFront();
    		stage.show();
    		controller.doChart();
    		
    	} catch(Exception e){
    		e.printStackTrace();
    	}
     }
    
    /** Method for stopping current plotting
     * and getting an option to save data 
     * into the database*/
    @FXML
    void buttonStopOnAction(ActionEvent event) {
    	// disabling the button to prevent abuse
    	buttonStop.setDisable(true);
    	
    	// if the threads are alive stop them and remove the
    	// charts from layouts and also close the port
    	if (sdtTemp.isAlive() && sdtHum.isAlive() && sdtLux.isAlive()){
    		System.out.println("Stopping threads!");
    		sdtTemp.stop();
    		sdtHum.stop();
    		sdtLux.stop();
    		boxTemp.getChildren().remove(2);
    		boxHum.getChildren().remove(2);
    		boxLux.getChildren().remove(2);
    		chosenPort.closePort();
    	}
    	else{
    		// else get an alert that something is wrong
			Alert alert = new Alert(AlertType.ERROR);
	    	alert.setHeaderText("Threads are not active. Check port settings.");
	    	alert.showAndWait();
			
    	}
    	
    	// confirmation alert with two choices
    	// either OK to save to database
    	// or CANCEL empty the lists
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setHeaderText("Would you like to save current data?");
    	Optional<ButtonType> result = alert.showAndWait();
    	if(!result.isPresent()){
    	    // alert is exited, no button has been pressed.
    	}
    	else if(result.get() == ButtonType.OK){
    		progressIndicator.setProgress(0);
    	     //button okay has been pressed, save data to database.
    		
    		// hibernate objects for DB
    		final Session session;
        	Transaction transaction = null;
        	session = Main.getSession();
        	
        	// indicator control showing progress (doesn't seem to work when there's little data)
        	progressIndicator.setProgress(0.25);
        	
        	// 3 different sessions for saving data into database
        	try{
        		transaction = session.beginTransaction();
        		for (DataStorage ds: sdtTemp.getDataList())
        		{
        			session.save(ds);
        		}
        		
        		transaction.commit();
        		progressIndicator.setProgress(0.5);
        	}catch(HibernateException e){
        			e.getCause();
        			transaction.rollback();
        	} 
        	try{
        		transaction = session.beginTransaction();
        		for (DataStorage ds: sdtHum.getDataList())
        		{
        			session.save(ds);
        		}
        		transaction.commit();
        		progressIndicator.setProgress(0.75);
        	}catch(HibernateException e){
        			e.getCause();
        			transaction.rollback();
        	} 
        	try{
        		transaction = session.beginTransaction();
        		for (DataStorage ds: sdtLux.getDataList())
        		{
        			session.save(ds);
        		}
        		
        		transaction.commit();
        		progressIndicator.setProgress(1);
        	}catch(HibernateException e){
        			e.getCause();
        			transaction.rollback();
        	} 
    	}
    	else if(result.get() == ButtonType.CANCEL){
    	    // cancel button is pressed - clear the lists.
    		sdtTemp.getDataList().clear();
    		sdtHum.getDataList().clear();
    		sdtLux.getDataList().clear();
    	}
    	buttonGo.setDisable(false);
    }

    /** Method used for starting
     * all threads and live plotting*/
    @FXML
    void buttonGoOnAction(ActionEvent event) {
    	progressIndicator.setProgress(0);
    	// disabling button to prevent abuse
    	buttonGo.setDisable(true);
    	
    	// trying to get selected port
    	try{
    	chosenPort = SerialPort.getCommPort(choiceBoxPort.getSelectionModel().getSelectedItem());
    	chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0); 
    	} catch (Exception e){
    		// getting an alert if it is impossible to get port
    		e.getMessage();
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setHeaderText("Port selection error!");
    		alert.showAndWait();
    	}
    	
    	if (chosenPort.openPort()){
    	// if port opening is successful create Data transfer objects
    	sdtTemp = new SerialDataTransferTemperature(chosenPort, boxTemp, lock, labelTemp);
    	sdtHum = new SerialDataTransferHumidity(chosenPort, boxHum, lock, labelHum);
    	sdtLux = new SerialDataTransferLux(chosenPort, boxLux, lock, labelLux);
    	
    	// starting the threads
    	sdtTemp.start();
        sdtHum.start();
        sdtLux.start();

    	} else{
    		// getting an error if port is not opened
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setHeaderText("Error while opening port!");
    		alert.showAndWait();
    	}
    	
    	// enabling the STOP button if everything went accordingly
    	buttonStop.setDisable(false);
    }

	public Label getLabelTemp() {
		return labelTemp;
	}

	public void setLabelTemp(Label labelTemp) {
		this.labelTemp = labelTemp;
	}

	public Label getLabelHum() {
		return labelHum;
	}

	public void setLabelHum(Label labelHum) {
		this.labelHum = labelHum;
	}

	public Label getLabelLux() {
		return labelLux;
	}

	public void setLabelLux(Label labelLux) {
		this.labelLux = labelLux;
	}

	public VBox getBoxTemp() {
		return boxTemp;
	}

	public void setBoxTemp(VBox boxTemp) {
		this.boxTemp = boxTemp;
	}

	public VBox getBoxHum() {
		return boxHum;
	}

	public void setBoxHum(VBox boxHum) {
		this.boxHum = boxHum;
	}

	public VBox getBoxLux() {
		return boxLux;
	}

	public void setBoxLux(VBox boxLux) {
		this.boxLux = boxLux;
	}

	public Scene getMainScene() {
		return mainScene;
	}

	public void setMainScene(Scene mainScene) {
		this.mainScene = mainScene;
	}

	public SerialPort getChosenPort() {
		return chosenPort;
	}
    
    

}

