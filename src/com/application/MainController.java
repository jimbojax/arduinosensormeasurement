package com.application;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainController {

	/** FXML fields */
    @FXML
    private BorderPane root;
    @FXML
    private HBox hBoxRoot;
    @FXML
    private VBox rootOne;
    @FXML
    private Label labelTemp;
    @FXML
    private VBox rootTwo;
    @FXML
    private Label labelHum;
    @FXML
    private VBox rootThree;
    @FXML
    private Label labelLux;
    @FXML
    private MenuItem menuItemConnect;
    @FXML
    private MenuItem menuItemClose;

    /** Method for closing the application */
    @FXML
    void menuItemCloseOnAction(ActionEvent event) {
    	Platform.exit();
    }

    /** Method used to open a window with settings */
    @FXML
    void menuItemConnectOnAction(ActionEvent event) {

    	try{
    		Stage stage = new Stage();
    		FXMLLoader loader = new FXMLLoader();
    		TabPane form = loader.load(getClass().getResource("/com/application/Connect.fxml").openStream());
    		ConnectController controller = (ConnectController) loader.getController();
    		// checking if this stage has been closed and re-opened again
    		// if it has, previous charts are removed
    		if (rootOne.getChildren().size() == 3){
        		rootOne.getChildren().remove(2);
        		rootTwo.getChildren().remove(2);
        		rootThree.getChildren().remove(2);
        	}
    		// setting labels and layouts to hierarchy above
    		controller.setLabelTemp(labelTemp);
    		controller.setLabelHum(labelHum);
    		controller.setLabelLux(labelLux);
    		controller.setBoxTemp(rootOne);
    		controller.setBoxHum(rootTwo);
    		controller.setBoxLux(rootThree);
        	
    		// setting scene and stage
    		Scene scene = new Scene(form);
    		controller.setMainScene(scene);
    		stage.setScene(scene);
    		stage.setTitle("Settings");
    		stage.toFront();
    		stage.show();
    		
    		// if a stage is closed the menu item is re-enabled
    		// and if a port has been opened it is now closed
    		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent event) {
                	menuItemConnect.setDisable(false);
                	if (controller.getChosenPort().isOpen()){
                	controller.getChosenPort().closePort();
                	}
                }
            });
    		
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	// disabling the menu item to prevent abuse
    	menuItemConnect.setDisable(true);
    }

}
