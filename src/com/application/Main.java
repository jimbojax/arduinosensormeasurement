package com.application;
	
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.storage.DataStorage;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	
	// static session factory for producing sessions
	private static final SessionFactory ourSessionFactory;

	static {
		try {
			// setting up the hibernate config file
			Configuration configuration = new Configuration();
			configuration.configure("/com/hibernate/hibernate.cfg.xml");

			// annotating the DataStorage class
			configuration.addAnnotatedClass(DataStorage.class);

			// applying configuration
			StandardServiceRegistryBuilder serviceBuilder = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties());

			// applying configuration to session factory
			ourSessionFactory = configuration.buildSessionFactory(serviceBuilder.build());

		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	/** Static method for getting a new session */
	public static Session getSession() throws HibernateException {
		return ourSessionFactory.openSession();
	}
	
	
	
	@Override
	public void start(Stage primaryStage) {
		// loading the main stage
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/com/application/Main.fxml"));
			Scene scene = new Scene(root,900,480);
			//scene.getStylesheets().add(getClass().getResource("/com/application/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
