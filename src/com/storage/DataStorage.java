package com.storage;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="_datastorage")
public class DataStorage {
	
	//Sensor parameter and time/date stamp
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="_id")
    @Id
	private int id;
	@Column(name="_data")
	private double data;
	@Column(name="_type")
	private String type;
	@Column(name="_dateTime")
	private LocalDateTime dateTime;
	
	public DataStorage(double _data, String _type)
	{
		data = _data;
		type = _type;
		dateTime = LocalDateTime.now();
	}
	
	public DataStorage(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getData() {
		return data;
	}

	public void setData(double data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public String toString() {
		return dateTime.toString().substring(11);
	}

	

	
	
}
