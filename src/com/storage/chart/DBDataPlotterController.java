package com.storage.chart;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.storage.DataStorage;

public class DBDataPlotterController {

	/** FXML fields */
    @FXML
    private LineChart<String, Number> lineChart;

    /** Non-fxml fields */
    private XYChart.Series<String, Number> series;
    private List<DataStorage> list;
    private DataStorage startTimeSelected;
    private DataStorage endTimeSelected;
    
    
    @FXML
    void initialize(){
    	list = new ArrayList<>();
    }

    public void doChart(){
    	series = new XYChart.Series<>();
    	
    	for (DataStorage ds: list){
    	//adding data into series
    		if (ds.getId()>=startTimeSelected.getId() && ds.getId()<=endTimeSelected.getId()){
    				series.getData().add(new XYChart.Data<String, Number>(ds.toString(), ds.getData()));
    		}
    			
    	}
    	
    	// turning off animation
    	lineChart.setAnimated(false);
    	
    	// applying series data to chart
    	lineChart.getData().add(series);
    }


	public List<DataStorage> getList() {
		return list;
	}


	public void setList(List<DataStorage> list) {
		this.list = list;
	}


	public DataStorage getStartTimeSelected() {
		return startTimeSelected;
	}


	public void setStartTimeSelected(DataStorage startTimeSelected) {
		this.startTimeSelected = startTimeSelected;
	}


	public DataStorage getEndTimeSelected() {
		return endTimeSelected;
	}


	public void setEndTimeSelected(DataStorage endTimeSelected) {
		this.endTimeSelected = endTimeSelected;
	}


	


	
    
    
}

