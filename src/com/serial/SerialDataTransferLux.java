package com.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.sun.org.apache.xpath.internal.SourceTree;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.plotter.PlotterLux;
import com.storage.DataStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ElFuego on 5/21/2017.
 */
public class SerialDataTransferLux extends Thread {

    private SerialPort selectedPort;
    private double number;
    private Scanner scanner;
    private PlotterLux plotterLux;
    private Object lock;
    private List<DataStorage> dataList;
    private final String type = "Lux";

    public SerialDataTransferLux(SerialPort input, VBox root, Object l, Label _lbl) {
    	// instantiating plotter
        plotterLux = new PlotterLux(root, _lbl);
        
        // getting the selected port
        selectedPort = input;
        
        // instantiating scanner and getting stream of data from the selected port using Scanner object
        scanner = new Scanner(selectedPort.getInputStream());
        
        // lock object for Mutual Exclusion
        lock = l;
        
        // instantiating DataStorage list for storing data that goes into database later
        dataList = new ArrayList<DataStorage>();
    }

    /** Method run that is extended from Thread class
     * and synchronizing the thread with other thread
     * using lock object.
     * Method then checks the string and parses it to double
     * and calls the plotter's method to update the chart.
     * Also the method stores data into the DataStorage list
     * and then the lock notifies the other threads that it 
     * has been unlocked so the other threads can use the data stream*/
    @Override
    public void run() {
        synchronized (lock){
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.charAt(0) == 'P') {
                line = line.replaceAll("P=","");
                number = Double.parseDouble(line);
                plotterLux.updatePlotLux(number);
                dataList.add(new DataStorage(number, type));
                lock.notifyAll();
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}



	public List<DataStorage> getDataList() {
		return dataList;
	}



	public PlotterLux getPlotterLux() {
		return plotterLux;
	}
    
    
}
